---
created: 2021-04-10T17:41:02-04:00
lastmodified: 2021-04-11T05:36:58-04:00
title: Workshops Resources
type: Checklist
tags: [Strezless Musick Sitemapping]
---

- [ ] https://www.atlassian.com/team-playbook/plays?attribute=&level=
- [ ] https://tettra.com
- [ ] https://github.com/blockchain/blockchain-wallet-v4-frontend